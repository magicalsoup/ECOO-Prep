## Anaylsis

This problem is pure implmentation. Simply find the words and fill out the rows, columns, and diagonals. Then append the character that were not chosen in to the answer string.

**Time Complexity:** $`O(R \times C)`$