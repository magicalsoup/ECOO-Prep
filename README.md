# ECOO

https://magicalsoup.github.io/ECOO-Prep/

# Progress

## 2012
|Round|Problem #|Completed?|
|:----|:--------|:---------|
|  1  |    1    |    ✔    |
|  1  |    2    |    ✔    |
|  1  |    3    |    ✔    |
|  1  |    4    |    ✔    |
|Final|         |    ✔    |

## 2013
|Round|Problem #|Completed?|
|:----|:--------|:---------|
|  1  |    1    |    ✔    |
|  1  |    2    |    ✔    |
|  1  |    3    |    ✔    |
|  1  |    4    |   ❌    |
|Final|         |    3/4    |

## 2014
|Round|Problem #|Completed?|
|:----|:--------|:---------|
|  1  |    1    |    ✔    |
|  1  |    2    |    ❌    |
|  1  |    3    |   ❌    |
|  1  |    4    |   ❌    |
|Final|         |    1/4    |

## 2015
|Round|Problem #|Completed?|
|:----|:--------|:---------|
|  1  |    1    |    ✔    |
|  1  |    2    |    ❌ (James is doing it)    |
|  1  |    3    |   ❌    |
|  1  |    4    |   ❌    |
|Final|         |    1/4    |

## 2016
|Round|Problem #|Completed?|
|:----|:--------|:---------|
|  1  |    1    |    ✔    |
|  1  |    2    |    ✔    |
|  1  |    3    |   ❌    |
|  1  |    4    |   ❌    |
|Final|         |    2/4    |

## 2017
|Round|Problem #|Completed?|
|:----|:--------|:---------|
|  1  |    1    |    ✔    |
|  1  |    2    |    ❌    |
|  1  |    3    |   ❌    |
|  1  |    4    |   ✔     |
|Final|         |    2/4    |

## 2018
|Round|Problem #|Completed?|
|:----|:--------|:---------|
|  1  |    1    |     ✔  |
|  1  |    2    |    ❌    |
|  1  |    3    |   ❌    |
|  1  |    4    |   ❌    |
|Final|         |    1/4    |


